// Set up an enum of valid contract sides.
export type ContractSideV1 = 'hedge' | 'long';

/**
 * Information about service fees.
 */
export type ContractFeeV1 =
{
	/** name or identifier for the fee in form presentable to an end-user. */
	name: string;

	/** description explaining the fee in form presentable to an end-user. */
	description: string;

	/** bitcoin cash address where service fees are to be paid. */
	address: string;

	/** how many satoshis are required to be paid as service fee. */
	satoshis: number;
};

/* */
export enum SettlementTypeV1
{
	MATURATION = 'maturation',
	LIQUIDATION = 'liquidation',
	MUTUAL = 'mutual',
}

export interface ContractPayoutV1
{
	settlementType: SettlementTypeV1;
	settlementTransactionHash: string;

	hedgePayoutInSatoshis: number;
	longPayoutInSatoshis: number;
}

export interface ContractAutomatedPayoutV1 extends ContractPayoutV1
{
	/** Oracle message containing the price used to determine the payout values for this settlement.  */
	settlementMessage: string;

	/** Oracle signature for the message containing the price used to determine the payout values for this settlement.  */
	settlementSignature: string;

	/** Oracle message sequentially before the settlement message used to prove that the settlement message is the first valid message to settle with.  */
	previousMessage: string;

	/** Oracle signature for the message used to prove that the settlement message is the first valid message to settle with.  */
	previousSignature: string;

	/** The price in oracle units that this settlement used to determine its payout value, stored for convenience. */
	settlementPrice: number;
}

export interface ContractMaturationV1 extends ContractAutomatedPayoutV1
{
	settlementType: SettlementTypeV1.MATURATION;
}

export interface ContractLiquidationV1 extends ContractAutomatedPayoutV1
{
	settlementType: SettlementTypeV1.LIQUIDATION;
}

export interface ContractMutualRedemptionV1 extends ContractPayoutV1
{
	settlementType: SettlementTypeV1.MUTUAL;
}
export type ContractSettlementV1 = ContractMaturationV1 | ContractLiquidationV1 | ContractMutualRedemptionV1;

/**
 * Funding and settlement information relating to an AnyHedge contract.
 */
export interface ContractFundingV1
{
	/** Funding transaction hash used to identify a funding of this contract. */
	fundingTransactionHash: string;

	/** Output in the funding transaction that sends to this contract. */
	fundingOutputIndex: number;

	/** Satoshis that is sent in this funding transaction to this contract. */
	fundingSatoshis: number;

	/** Settlement information if this funding has been settled on-chain. */
	settlement?: ContractSettlementV1;
}

/**
 * Parameters used directly inside the AnyHedge contract.
 */
export interface ContractParametersV1
{
	/** Public key of the oracle that provide price information to this contract. */
	oraclePublicKey: string;

	/** Lower price at which the contract liquidates at. */
	lowLiquidationPrice: number;

	/** Upper price at which the contract liquidates at. */
	highLiquidationPrice: number;

	/**  Unix timestamps of the earliest data and time the contract can be liquidated. */
	startTimestamp: number;

	/** Unix timestamp of the date and time at which the contract matures. */
	maturityTimestamp: number;

	/** Nominal contract size scaled by satoshis-per-bch in order to retain precision of internal calculations. */
	nominalUnitsXSatsPerBch: number;

	/** Total satoshis sent when funding, minus a predetermined miner-fee, used to determine which satoshis to pay out on settlement. */
	payoutSats: number;

	/** Payout lockscript used to encumber funds paid out from the contract to the hedge position. */
	hedgeLockScript: string;

	/** Payout lockscript used to encumber funds paid out from the contract to the long position. */
	longLockScript: string;

	/** Integer flag that can be used to enable or disable mutual redemption support in the contract. (0 = disabled, 1 = enabled) */
	enableMutualRedemption: number;

	/** Public key for the hedge positions keypair used for mutual redemption. */
	hedgeMutualRedeemPublicKey: string;

	/** Public key for the long positions keypair used for mutual redemption. */
	longMutualRedeemPublicKey: string;
}

/**
 * Metadata not directly used by the AnyHedge contract, but used in debugging and in many utility functions that require parameters closer to intent.
 */
export interface ContractMetadataV1
{
	/** Which side of the contract that the initiating party takes on. */
	takerSide: ContractSideV1;

	/** Which side of the contract that the responding party takes on. */
	makerSide: ContractSideV1;

	/** Payout address where the hedge position of the contract pays out to on settlement. */
	hedgePayoutAddress: string;

	/** Payout address where the long position of the contract pays out to on settlement. */
	longPayoutAddress: string;

	/** Oracle message used as the starting point for the contract. */
	startingOracleMessage: string;

	/** Oracle signature for the message used as the starting point for the contract. */
	startingOracleSignature: string;

	/** Oracle price used to determine starting conditions for the contract. */
	startPrice: number;

	/** Duration in seconds in relation to the starting timestamp used to determine the contracts maturity time. */
	durationInSeconds: number;

	/** Nominal size of the contract, equivalent to the hedge positions input units. */
	nominalUnits: number;

	/** Multiplier for startPrice that determines the lower liquidation price, and therefor both the leverage and price protection of the contract. */
	lowLiquidationPriceMultiplier: number;

	/** Multiplier for startPrice that determines the upper liquidation price. */
	highLiquidationPriceMultiplier: number;

	/**  Initial size of the hedge position in the contract, in oracle units. */
	hedgeInputInOracleUnits: number;

	/**  Initial size of the long position in the contract, in oracle units. */
	longInputInOracleUnits: number;

	/**  Initial size of the hedge position in the contract, in satoshis. */
	hedgeInputInSatoshis: number;

	/**  Initial size of the long position in the contract, in satoshis. */
	longInputInSatoshis: number;

	/** Number of satoshis to reserve for settlement miner fees. */
	minerCostInSatoshis: number;
}

/**
 * Detailed description of an AnyHedge contract, including all necessary parameters to recreate and validate the contract.
 */
export interface ContractDataV1
{
	/** Contract version, used to identify which contract lockscript is used. */
	version: string;

	/** Contract address, used to identify contract interactions in block explorers and acts as a checksum for contract parameterization. */
	address: string;

	/** Contract fees, used for coordination and follow-up by the various services that helped the contract parties fund and/or redeem contracts. */
	fees: ContractFeeV1[];

	/** Contract parameters, used in the lock and unlock script and defines the configurable parts of the contract behaviour. */
	parameters: ContractParametersV1;

	/** Contract metadata, used to store the original contract intent and environment as well as in many utility functions. */
	metadata: ContractMetadataV1;

	/** Contract funding, used to track all funded instances of the contract. */
	fundings: ContractFundingV1[];
}
