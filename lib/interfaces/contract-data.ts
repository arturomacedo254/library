import { ContractDataV1, ContractMetadataV1, ContractParametersV1, ContractFundingV1, ContractSettlementV1, ContractFeeV1, ContractSideV1, SettlementTypeV1, ContractAutomatedPayoutV1, ContractMaturationV1, ContractLiquidationV1 } from './versions/v1/index.js';

// Combine the interface of different versions
export type ContractFee = ContractFeeV1;
export type ContractParameters = ContractParametersV1;
export type ContractMetadata = ContractMetadataV1;
export type ContractFunding = ContractFundingV1;
export type ContractSettlement = ContractSettlementV1;
export type ContractData = ContractDataV1;

// ...
export type ContractSide = ContractSideV1;
export type ContractMaturation = ContractMaturationV1;
export type ContractLiquidation = ContractLiquidationV1;
export type ContractAutomatedPayout = ContractAutomatedPayoutV1;

// Re-export the settlement types as an object.
export const SettlementType = SettlementTypeV1;
