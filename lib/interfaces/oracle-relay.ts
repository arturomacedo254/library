 /**
 * Configuration parameters used to communicate with a oracle relay service.
 *
 * @todo move this to the oracle library.
 */
export interface OracleRelayConfiguration
{
	/** The host (domain) of the Service. */
	host: string;

	/** The port number of the Service. */
	port: number;

	/** The scheme (http or https) of the Service. */
	scheme: 'http' | 'https';
}
