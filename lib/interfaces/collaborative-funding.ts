import { Utxo } from 'cashscript';
import { ContractData } from './contract-data.js';

export interface UnsignedFundingProposal
{
	contractData: ContractData;
	utxo: Utxo;
}

export interface SignedFundingProposal extends UnsignedFundingProposal
{
	publicKey: string;
	signature: string;
}
