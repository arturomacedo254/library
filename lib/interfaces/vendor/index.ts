import { OracleData } from '@generalprotocols/price-oracle';

export type OraclePriceMessage = ReturnType<typeof OracleData.parsePriceMessage>;
