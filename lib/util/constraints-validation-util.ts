import { DUST_LIMIT, MAX_CONTRACT_SATS, MIN_INTEGER_DIVISION_PRECISION_STEPS, SATS_PER_BCH, JAVASCRIPT_FRIENDLY_SCRIPT_INT_MAX, MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE } from '../constants.js';
import { HighLiquidationPriceError, LowLiquidationPriceError, NominalUnitsXSatsPerBchError, PayoutSatsError } from '../errors.js';

const messageForUnactionableErrors = 'Unexpected error. Please file a bug with contract parameters at https://gitlab.com/GeneralProtocols/anyhedge/library.';

/**
 * Validates minimum integer division precision, which is used in various contract calculations.
 * Precision is defined in the context of integer division when the remainder portion (mod) is discarded.
 * Precision is fine when the relative distance between numbers is large, e.g. (1_000_000 / 7 = 142857 vs. 142857.142).
 * However, calculations become imprecise when the relative distance is small, e.g. (10 / 7 = 1 vs. 1.429, almost 50% error).
 *
 * @param positiveIntegerNumerator         {number} positive integer numerator value.
 * @param positiveIntegerDenominator       {number} positive integer denominator value.
 * @param minIntegerDivisionPrecisionSteps {number} positive integer required steps.
 *
 * @throws {Error} if validation fails.
 * @returns {Promise<void>}
 */
export const validateIntegerDivisionPrecision = async function(
	positiveIntegerNumerator: number,
	positiveIntegerDenominator: number,
	minIntegerDivisionPrecisionSteps: number,
): Promise<void>
{
	// This measurement is only defined when all arguments are positive integers.
	// Validate numerator as a positive integer.
	const numeratorIsPositiveInteger = (Number.isInteger(positiveIntegerNumerator) && (positiveIntegerNumerator >= 1));
	if(!numeratorIsPositiveInteger)
	{
		throw(new Error(`Numerator must be a positive integer but got ${positiveIntegerNumerator}.`));
	}

	// Also validate denominator as a positive integer.
	const denominatorIsPositiveInteger = (Number.isInteger(positiveIntegerDenominator) && (positiveIntegerDenominator >= 1));
	if(!denominatorIsPositiveInteger)
	{
		throw(new Error(`Denominator must be a positive integer but got ${positiveIntegerDenominator}.`));
	}

	// Also validate minimum precision steps as a positive integer.
	const precisionIsPositiveInteger = (Number.isInteger(minIntegerDivisionPrecisionSteps) && (minIntegerDivisionPrecisionSteps >= 1));
	if(!precisionIsPositiveInteger)
	{
		throw(new Error(`Division precision steps must be a positive integer but got ${minIntegerDivisionPrecisionSteps}.`));
	}

	// Calculate Real valued division steps between numerator and denominator
	const actualDivisionSteps = positiveIntegerNumerator / positiveIntegerDenominator;

	// Confirm that there are at least as many steps as the minimum requirement
	if(!(actualDivisionSteps >= minIntegerDivisionPrecisionSteps))
	{
		// The relative distance was not large enough, so prepare some helpful numbers for a message and throw an error.

		// Represent the actual precision as a percent string
		const actualPrecisionPercentString = (100 * (1 / actualDivisionSteps)).toFixed(2);

		// Represent the minimum precision as a percent string
		const minimumPrecisionPercentString = (100 * (1 / minIntegerDivisionPrecisionSteps)).toFixed(2);

		// Describe the problem in a context-independent way.
		const message = `The division (${positiveIntegerNumerator} / ${positiveIntegerDenominator}) must have at most ${minimumPrecisionPercentString}% error (at least ${minIntegerDivisionPrecisionSteps} steps). Actual worst case precision is ${actualPrecisionPercentString}% (${actualDivisionSteps.toFixed(1)} steps).`;

		// Throw an error that describes the problem.
		throw(new Error(message));
	}
};

/*
	Validators in this group are for high level specific values that need to be reported in custom errors so that
	they are more actionable for application builders.
*/

/**
 * Validates analytical constraints on the high liquidation price in the context of a full contract lifecycle.
 *
 * @param highLiquidationPrice {number} high liquidation price stored in a contract's initial data.
 * @param startPrice           {number} start price used to calculate various contract parameters.
 *
 * @throws {HighLiquidationPriceError} if validation fails.
 * @returns {Promise<void>}
 */
export const validateHighLiquidationPrice = async function(
	highLiquidationPrice: number,
	startPrice: number,
): Promise<void>
{
	// High liquidation price is a contract value and therefore must be an integer.
	// Any non-integer values indicate a problem in the contract preparation process.
	if(!Number.isInteger(highLiquidationPrice))
	{
		throw(new HighLiquidationPriceError(
			`High liquidation price must be an integer to be valid for use in contracts, but got ${highLiquidationPrice}.`,
			messageForUnactionableErrors,
		));
	}

	//
	// Validate Maximum Constraints for highLiquidationPrice
	//

	// Note: The maximum is effectively set by {DivPrecision} on nominalUnitsXSatsPerBch. Validating the same limit here
	//       would require a redundant test, making one of them effectively unreachable.
	//       Therefore, there are no maximum tests here.

	//
	// Validate Minimum Constraints for highLiquidationPrice
	//

	// High liquidation price must be greater than the start price (ceil is conservative) to avoid instant liquidation.
	// Although the policy boundary for a minimum gap between start price and high liquidation price exists, this is
	// a strict safety boundary that must be kept.
	// Root name in constraints diagram: {ExclusivePriceSequence}
	if(!(highLiquidationPrice > Math.ceil(startPrice)))
	{
		throw(new HighLiquidationPriceError(
			`High liquidation price must be greater than ceiling of start price (ceil(${startPrice})) in order to avoid instant liquidation, but got ${highLiquidationPrice}.`,
			'Increase high liquidation price multiplier.',
		));
	}

	// High liquidation price must have enough space between it and the start price for contracts to avoid near-instant liquidation.
	// Note: This is a policy constraint rather than a safety constraint.
	const minimumHighLiquidationPrice = Math.ceil(startPrice * (1 + MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE));
	if(!(highLiquidationPrice >= minimumHighLiquidationPrice))
	{
		throw(new HighLiquidationPriceError(
			`High liquidation price must be sufficiently greater than start price (${startPrice}) in order to avoid near-instant liquidation. Expected at least ${minimumHighLiquidationPrice} but got ${highLiquidationPrice}.`,
			'Increase high liquidation price multiplier.',
		));
	}
};

/**
 * Validates analytical constraints on the low liquidation price in the context of a full contract lifecycle.
 *
 * @param lowLiquidationPrice {number} low liquidation price stored in a contract's initial data.
 * @param startPrice          {number} start price used to calculate various contract parameters.
 *
 * @throws {LowLiquidationPriceError} if validation fails.
 * @returns {Promise<void>}
 */
export const validateLowLiquidationPrice = async function(
	lowLiquidationPrice: number,
	startPrice: number,
): Promise<void>
{
	// Low liquidation price is a contract value and therefore must be an integer.
	// Any non-integer values indicate a problem in the contract preparation process.
	if(!Number.isInteger(lowLiquidationPrice))
	{
		throw(new LowLiquidationPriceError(
			`Low liquidation price must be an integer to be valid for use in contracts, but got ${lowLiquidationPrice}.`,
			messageForUnactionableErrors,
		));
	}

	//
	// Validate Maximum Constraints for lowLiquidationPrice
	//

	// Low liquidation price must be less than the start price (floor is conservative) to avoid instant liquidation.
	// Although the policy boundary for a minimum gap between start price and low liquidation price exists, this is
	// a strict safety boundary that must be kept.
	// Root name in constraints diagram: {ExclusivePriceSequence}
	if(!(lowLiquidationPrice < Math.floor(startPrice)))
	{
		throw(new LowLiquidationPriceError(
			`Low liquidation price must be less than floor of start price (floor(${startPrice})) in order to avoid instant liquidation, but got ${lowLiquidationPrice}.`,
			'Reduce low liquidation price multiplier / increase price protection.',
		));
	}

	// Low liquidation price must have enough space between it and the start price for contracts to avoid near-instant liquidation.
	// Note: This is a policy constraint rather than a safety constraint, and does not appear in the constraints diagram.
	const lowLiquidationPriceWithReasonableGapToStartPrice = Math.floor(startPrice * (1 - MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE));
	if(!(lowLiquidationPrice <= lowLiquidationPriceWithReasonableGapToStartPrice))
	{
		throw(new LowLiquidationPriceError(
			`Low liquidation price must be sufficiently less than start price (${startPrice}) in order to avoid near-instant liquidation. Expected at most ${lowLiquidationPriceWithReasonableGapToStartPrice} but got ${lowLiquidationPrice}.`,
			'Reduce low liquidation price multiplier / increase price protection.',
		));
	}

	//
	// Validate Minimum Constraints for lowLiquidationPrice
	//

	// Low liquidation price must be at least 1 to avoid a divide-by-zero error leading to a potentially unredeemable contract.
	// Root name in constraints diagram: {ExclusivePriceSequence}
	if(!(lowLiquidationPrice >= 1))
	{
		throw(new LowLiquidationPriceError(
			`Low liquidation price must be at least 1 but got ${lowLiquidationPrice}.`,
			'Increase low liquidation price multiplier / reduce price protection.',
		));
	}

	// Note: Another minimum for this value is effectively set by {LongSatsUnsafe>=0} on nominalSatsXSatsPerBch. Validating the same
	//       limit  which would require a redundant test, making one of them effectively unreachable.
	//       Therefore, the additional minimum is not tested here.
};

/**
 * Validates analytical constraints on the compound nominal value x satoshis per bch in the context of a full contract lifecycle.
 *
 * @param nominalUnitsXSatsPerBch {number} compound nominal value stored in a contract's initial data.
 * @param highLiquidationPrice    {number} high liquidation price stored in a contract's initial data.
 * @param lowLiquidationPrice     {number} low liquidation price stored in a contract's initial data.
 * @param payoutSats              {number} total payout satoshis stored in a contract's initial data.
 *
 * @throws {NominalUnitsXSatsPerBchError} if validation fails.
 * @returns {Promise<void>}
 */
export const validateNominalUnitsXSatsPerBch = async function(
	nominalUnitsXSatsPerBch: number,
	highLiquidationPrice: number,
	lowLiquidationPrice: number,
	payoutSats: number,
): Promise<void>
{
	// The compound nominal value is a contract value and therefore must be an integer.
	// Any non-integer values indicate a problem in the contract preparation process.
	if(!Number.isInteger(nominalUnitsXSatsPerBch))
	{
		throw(new NominalUnitsXSatsPerBchError(
			`The compound nominal value must be an integer to be valid for use in contracts, but got ${nominalUnitsXSatsPerBch}.`,
			messageForUnactionableErrors,
		));
	}

	//
	// Validate Maximum Constraints for nominalUnitsXSatsPerBch
	//

	// The compound nominal value must be less than the maximum script integer.
	// Note: The javascript friendly limit is drastically below the actual limit, but all messages are presented
	// in terms of the actual hard limit in order to preserve the critical nature of the constraint.
	// Root name in constraints diagram: {MaxNominalUnitsXSatsPerBchAsHedgeSatsUnsafeInput}
	if(!(nominalUnitsXSatsPerBch <= JAVASCRIPT_FRIENDLY_SCRIPT_INT_MAX))
	{
		throw(new NominalUnitsXSatsPerBchError(
			`The compound nominal value must be at most the maximum allowed script integer (${JAVASCRIPT_FRIENDLY_SCRIPT_INT_MAX}), but got ${nominalUnitsXSatsPerBch}.`,
			'Reduce nominal contract value.',
		));
	}

	// The compound nominal value is part of the key constraint that basically defines an AnyHedge contract,
	// i.e. at low liquidation, long payout = full payout.
	// Note: This is a sanity check only. If it fails, something in contract preparation has gone seriously wrong.
	// Root name in constraints diagram: {LongSatsUnsafe>=0}
	const reverseCalculatedCompoundValue = payoutSats * lowLiquidationPrice;
	if(!(nominalUnitsXSatsPerBch <= reverseCalculatedCompoundValue))
	{
		throw(new NominalUnitsXSatsPerBchError(
			`There is a mismatch where (payoutSats * lowLiquidationPrice) (${reverseCalculatedCompoundValue}) is less than the actual compound nominal value (${nominalUnitsXSatsPerBch}), meaning the contract will not be able to pay out at low liquidation.`,
			messageForUnactionableErrors,
		));
	}

	//
	// Validate Minimum Constraints for nominalUnitsXSatsPerBch
	//

	// The compound nominal value represents (within rounding) nominalUnits * SATS_PER_BCH. A semi-policy,
	// semi-safety assertion is that nominalUnits should be at least 1. Then this value must be at least SATS_PER_BCH.
	// Root name in constraints diagram: {NominalUnits>=1}
	if(!(nominalUnitsXSatsPerBch >= SATS_PER_BCH))
	{
		throw(new NominalUnitsXSatsPerBchError(
			`The compound nominal value must be at least 1e8 (i.e. sats per bch) in order to reflect the constraint that nominal units must be at least 1, but got ${nominalUnitsXSatsPerBch}.`,
			'Ensure that nominal units is at least 1.',
		));
	}

	// The compound nominal value must have enough space between it and maximum price to ensure division precision.
	// Practically speaking, this sets a minimum boundary for the compound nominal value.
	// Note: The +1 is a term that arises from worst-case evaluation of boundary conditions in rounding.
	//       Its impact is small when minimum precision steps is a realistic size.
	// Root name in constraints diagram: {DivPrecision}
	try
	{
		// Test division precision
		await validateIntegerDivisionPrecision(
			nominalUnitsXSatsPerBch,
			highLiquidationPrice,
			MIN_INTEGER_DIVISION_PRECISION_STEPS + 1,
		);
	}
	catch(error: any)
	{
		// If we had a division precision error, use division precision description with specific solutions.
		throw(new NominalUnitsXSatsPerBchError(
			`Regarding the division between compound nominal value and high liquidation price: ${error.message}`,
			'Increase nominal value. Reduce high liquidation price multiplier. Use an oracle with a smaller base unit.',
		));
	}
};

/**
 * Validates analytical constraints of payout satoshis in the context of a full contract lifecycle.
 *
 * @param payoutSats {number} total payout satoshis stored in a contract's initial data.
 *
 * @throws {PayoutSatsError} if validation fails.
 * @returns {Promise<void>}
 */
export const validatePayoutSats = async function(payoutSats: number): Promise<void>
{
	// Payout satoshis is a contract value and therefore must be an integer.
	// Any non-integer values indicate a problem in the contract preparation process.
	if(!Number.isInteger(payoutSats))
	{
		throw(new PayoutSatsError(
			`Payout satoshis must be an integer to be valid for use in contracts, but got ${payoutSats}.`,
			messageForUnactionableErrors,
		));
	}

	//
	// Validate Maximum Constraints for payoutSats
	//

	// Payout satoshis must be within the allowed total BCH value for contracts.
	// Root name in constraints diagram: {payoutSats<=MaxSats}
	if(!(payoutSats <= MAX_CONTRACT_SATS))
	{
		throw(new PayoutSatsError(
			`Payout satoshis must be at most the maximum allowed contract satoshis safety boundary (${MAX_CONTRACT_SATS}), but got ${payoutSats}.`,
			'Reduce nominal contract value. Increase low liquidation price multiplier / reduce price protection.',
		));
	}

	//
	// Validate Minimum Constraints for payoutSats
	//

	// Payout satoshis has a non-obvious reverse constraint, although it also looks logical, that it must be greater than DUST.
	// Root name in constraints diagram: {LongSats<PayoutSats}
	if(!(payoutSats > DUST_LIMIT))
	{
		throw(new PayoutSatsError(
			`Payout satoshis must be greater than the dust limit (${DUST_LIMIT}), but got ${payoutSats}.`,
			'Increase nominal contract value.',
		));
	}
};
