import { binToHex, hexToBin } from '@bitauth/libauth';
import debugLogger from 'debug';

// Initialize support for debug message management.
export const debug =
{
	action:	debugLogger('anyhedge:action'),
	object:	debugLogger('anyhedge:object'),
	errors:	debugLogger('anyhedge:errors'),
	params:	debugLogger('anyhedge:params'),
	result:	debugLogger('anyhedge:result'),
};

/**
* Utility function that creates a range of numbers
*
* @param stop   The first number outside of the range
*
* @returns an array with integers from 0 to `stop` (excluding `stop`)
* @private
*/
export const range = (stop: number): number[] => [ ...Array(stop).keys() ];

/**
 * Apply binToHex to all properties of an object.
 *
 * @param object   object with only Uint8Arrays as property values.
 *
 * @returns an object with all property values being hex strings
 * @private
 */
export const propertiesBinToHex = function(
	object: { [key: string]: Uint8Array },
): { [key: string]: string }
{
	// Loop over all key-value pairs in the object, apply binToHex to the values,
	// and add the new key-value pair to a result object.
	return Object.entries(object).reduce((accumulator, [ key, value ]) => ({ ...accumulator, [key]: binToHex(value) }), {});
};

/**
 * Apply hexToBin to all properties of an object
 *
 * @param object   object with only hex strings as property values
 *
 * @returns an object with all property values being Uint8Arrays
 * @private
 */
export const propertiesHexToBin = function(
	object: { [key: string]: string },
): { [key: string]: Uint8Array }
{
	// Loop over all key-value pairs in the object, apply hexToBin to the values,
	// and add the new key-value pair to a result object.
	return Object.entries(object).reduce((accumulator, [ key, value ]) => ({ ...accumulator, [key]: hexToBin(value) }), {});
};
