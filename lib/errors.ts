/* eslint-disable max-classes-per-file */

export class MissingAuthenticationTokenError extends Error
{
	constructor()
	{
		// Define a message indicating that no authentication token was provided.
		const message = 'No authentication token was provided. Please request one '
		+ 'using requestAuthenticationToken() and pass it into the AnyHedgeManager constructor.';

		// Create an instance of the parent Error class using the specified message.
		super(message);
	}
}

export class IncorrectWIFError extends Error
{
	constructor(privateKeyWIF: string)
	{
		// Define a message indicating that the WIF string has an incorrect format.
		const message = `Provided WIF string (${privateKeyWIF.substr(0, 4)}...) has an incorrect format`;

		// Create an instance of the parent Error class using the specified message.
		super(message);
	}
}

export class SettlementParseError extends Error
{
	constructor(reason: string)
	{
		// Define a message indicating that a settlement transaction could not be parsed.
		const message = `Settlement transaction could not be parsed: ${reason}`;

		// Create an instance of the parent Error class using the specified message.
		super(message);
	}
}

// Custom error that lets app developers recommend specific solutions to users.
class ErrorWithProblemAndSolutions extends Error
{
	constructor(problemSentences: string, potentialSolutionSentences: string)
	{
		// Use default error message style to combine problems and solutions
		const message = `Problem: ${problemSentences} Potential solutions: ${potentialSolutionSentences}`;

		// Create an instance of the parent Error class using the specified message.
		super(message);
	}
}

// Custom error for primary contract parameter: high liquidation price
export class HighLiquidationPriceError extends ErrorWithProblemAndSolutions
{ /* Only need a custom name. No need for behavior customization */ }

// Custom error for primary contract parameter: low liquidation price
export class LowLiquidationPriceError extends ErrorWithProblemAndSolutions
{ /* Only need a custom name. No need for behavior customization */ }

// Custom error for primary contract parameter: compound nominal value
export class NominalUnitsXSatsPerBchError extends ErrorWithProblemAndSolutions
{ /* Only need a custom name. No need for behavior customization */ }

// Custom error for primary contract parameter: payout satoshis
export class PayoutSatsError extends ErrorWithProblemAndSolutions
{ /* Only need a custom name. No need for behavior customization */ }
