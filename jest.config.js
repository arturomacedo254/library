export default {
	transform: {},
	reporters: [ 'default', 'jest-junit' ],
	testResultsProcessor: 'jest-junit',
	collectCoverageFrom: [
		'lib/**',
		'!lib/interfaces/**',
	],
	coverageReporters: [ 'text', 'lcov' ],
	projects: [
		{
			displayName: 'unit',
			testMatch: [ '<rootDir>/build/test/**/*.(spec|test).js' ],
			testPathIgnorePatterns: [ '/test/e2e/' ],
		},
		{
			displayName: 'e2e',
			testMatch: [ '<rootDir>/build/test/e2e/**/*.(spec|test).js' ],
		},
	],
};
