import { addressToPushPrefixedLockScript } from '../../lib/util/bitcoincash-util.js';

describe('addressToPushPrefixedLockScript()', () =>
{
	test('should generate a push-prefixed lock script for a P2SH address', async () =>
	{
		// Define a fixture P2SH address
		const address = 'bitcoincash:ppugxhq9tvczdhfc6fn0sah9z3pvgjf58v26eq40xq';

		// Derive its push-prefixed lock script
		const lockScript = addressToPushPrefixedLockScript(address);

		// Check that the derived lock script equals the expected lock script
		expect(lockScript).toEqual('17a91478835c055b3026dd38d266f876e51442c449343b87');
	});

	test('should generate a push-prefixed lock script for a P2PKH address', async () =>
	{
		// Define a fixture P2SH address
		const address = 'bitcoincash:qzjd2syewa5w82xt20z79lp2fku4nedqtvqrw4208s';

		// Derive its push-prefixed lock script
		const lockScript = addressToPushPrefixedLockScript(address);

		// Check that the derived lock script equals the expected lock script
		expect(lockScript).toEqual('1976a914a4d540997768e3a8cb53c5e2fc2a4db959e5a05b88ac');
	});
});
